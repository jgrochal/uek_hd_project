class Record:
    def __init__(self, params):
        self.auction_id = params.get('auction_id', None)
        self.brand = params.get('Marka pojazdu', None)
        self.model = params.get('Model pojazdu', None)
        self.category = params.get('Kategoria', None)
        self.year = params.get('Rok produkcji', None)
        self.price = params.get('price', None)
        self.mileage = params.get('Przebieg', None)
        self.engine = params.get('Pojemność skokowa', None)
        self.fuel = params.get('Rodzaj paliwa', None)
        self.location = params.get('location', None)
        self.dealer = params.get('Oferta od', None) == 'Firmy'
        self.origin_country = params.get('Kraj pochodzenia', None)
        self.power = params.get('Moc', None)
        self.transmission = params.get('Skrzynia biegów', None)
        self.type = params.get('Typ', None)
        self.doors = params.get('Liczba drzwi', None)
        self.seats = params.get('Liczba miejsc', None)
        self.color = params.get('Kolor', None)
        self.first_registered = params.get('Pierwsza rejestracja', None)
        self.first_owner = params.get('Pierwszy właściciel', None)
        self.no_accidents = params.get('Bezwypadkowy', None)
        self.state = params.get('Stan', None)
        self.abs = params.get('ABS', None)
        self.electric_windscreens = params.get('Elektryczne szyby przednie', None)
        self.power_steering = params.get('Wspomaganie kierownicy', None)
        self.cruise_control = params.get('Tempomat', None)
        self.gps = params.get('Nawigacja GPS', None)
        self.electric_side_mirrors = params.get('Elektrycznie ustawiane lusterka', None)
        self.alarm = params.get('Alarm', None)
        self.immobiliser = params.get('Immobilizer', None)
        self.driver_airbag = params.get('Poduszka powietrzna kierowcy', None)
        self.aux = params.get('Gniazdo AUX', None)
        self.bluetooth = params.get('Bluetooth', None)
        self.radio = params.get('Radio fabryczne', None)

    def __str__(self):
        return str(self.__dict__)

    def items(self):
        return self.__dict__

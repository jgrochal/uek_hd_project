from globals import *
import requests


def assert_url_valid(url):
    response = requests.get(url)
    assert str(response.url).lower() == url.lower()


def assert_all_possible_urls_are_valid():
    for i in sorted(list(BRANDS.keys())):
        brand = BRANDS[i]
        temp_dict = eval('MODELS_' + brand.upper())
        for j in sorted(list(temp_dict.keys())):
            model = temp_dict[j].replace(' ', '-').replace('\'', '')
            url = 'https://www.otomoto.pl/osobowe/' + brand + '/' + model + '/'
            assert_url_valid(url)


def main():
    assert_all_possible_urls_are_valid()


if __name__ == '__main__':
    main()

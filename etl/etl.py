from bs4 import BeautifulSoup
from collections import OrderedDict
import requests
import pickle
from models import Record
from globals import *
import os
import sqlite3
from sqlite3 import Error
import time
import datetime
import csv


def log_and_quit():
    print('Koniec programu...')
    quit()


def get_links_from_one_page(response):
    soup = BeautifulSoup(response.content, "html.parser")
    return OrderedDict((listing['data-ad-id'], listing['data-href']) for listing in soup.find_all('article'))


def append_hyperlinks(all_hyperlinks, new_hyperlinks):
    all_hyperlinks.update(new_hyperlinks)


def get_all_links_required(n_records, brand, model):
    page = 1
    all_hyperlinks = OrderedDict()
    while len(all_hyperlinks) < n_records:
        url = 'https://www.otomoto.pl/osobowe/' + brand + '/' + model + '/?page=' + str(page)
        try:
            response = requests.get(url)
            if not str(response.url).lower().split('?')[0] == url.lower().split('?')[0]:
                print('Koniec stron z ofertami osiągnięty zanim udało się pobrać ich', n_records)
                break
            append_hyperlinks(all_hyperlinks, get_links_from_one_page(response))
            page += 1
        except requests.exceptions.RequestException as e:
            print('Błąd podczas połączenia z internetem:\n\t', e)
            log_and_quit()
    if len(all_hyperlinks) > n_records:
        all_hyperlinks = OrderedDict(list(all_hyperlinks.items())[:n_records])
    print('Zostanie pobranych plików źródłowych:', len(all_hyperlinks))
    return all_hyperlinks


def get_source_of_listing(url):
    try:
        response = requests.get(url)
        return str(BeautifulSoup(response.content, "html.parser"))
    except requests.exceptions.RequestException as e:
        print('Błąd podczas połączenia z internetem:\n\t', e)
        log_and_quit()


def get_sources_of_all_listings(hyperlinks):
    try:
        return {key: get_source_of_listing(hyperlinks[key]) for key in hyperlinks.keys()}
    except AttributeError:
        print('Błąd podczas pobierania plików źródłowych ofert.')
        log_and_quit()


def extract(n_records, brand, model):
    print('Rozpoczęto ekstrakcję', n_records, 'ofert dla samochodu:', brand, model)
    all_hyperlinks = get_all_links_required(n_records, brand, model)
    all_sources = get_sources_of_all_listings(all_hyperlinks)

    try:
        with open(EXTRACTED_DATA_FILENAME, 'wb') as handle:
            pickle.dump(all_sources, handle, protocol=pickle.HIGHEST_PROTOCOL)
        print('Zapisano wyekstraktowane dane do pliku.')
    except IOError as e:
        print('Błąd: problem przy zapisywaniu do pliku:\n\t', e)
        log_and_quit()


def get_offer_params_key(tag):
    return str(tag('span')[0].contents[0])


def get_offer_params_value(tag):
    div_tag = tag('div')[0]
    if len(div_tag('a')) > 0:
        return div_tag('a')[0].contents[0].strip()
    return div_tag.contents[0].strip()


def get_params(source):
    soup = BeautifulSoup(source, "html.parser")
    class_tag = [l for l in soup.find_all('li', {'class': 'offer-params__item'})]
    result = {}
    for tag in class_tag:
        result[get_offer_params_key(tag)] = get_offer_params_value(tag)
    return result


def get_features(source):
    soup = BeautifulSoup(source, "html.parser")
    return {l.contents[2].strip(): 'Tak' for l in soup.find_all('li', {'class': 'offer-features__item'})}


def get_price(source):
    soup = BeautifulSoup(source, "html.parser")
    span_tag = soup.find('span', {'class': 'offer-price__number'})
    return {'price': ' '.join([span_tag.contents[0].strip(), span_tag('span')[0].contents[0].strip()])}


def get_location(source):
    soup = BeautifulSoup(source, "html.parser")
    span_tag = soup.find('span', {'class': 'seller-box__seller-address__label'}).contents[0].strip()
    return {'location': span_tag}


def transform_one_source_to_record(auction_id, listing_source):
    listing_info = {'auction_id': auction_id}
    params = get_params(listing_source)
    listing_info.update(params)
    features = get_features(listing_source)
    listing_info.update(features)
    price = get_price(listing_source)
    listing_info.update(price)
    location = get_location(listing_source)
    listing_info.update(location)
    return Record(listing_info)


def transform():
    try:
        with open(EXTRACTED_DATA_FILENAME, 'rb') as handle:
            all_sources = pickle.load(handle)
        print('Rozpoczęto transformację danych.')
        try:
            records = [transform_one_source_to_record(source_id, all_sources[source_id]) for source_id in all_sources.keys()]
        except AttributeError as e:
            print('Błąd: nieprawidłowy plik wynikowy z ekstrakcji danych\n\t', e)
            log_and_quit()

        try:
            with open(TRANSFORMED_DATA_FILENAME, 'wb') as handle:
                pickle.dump(records, handle, protocol=pickle.HIGHEST_PROTOCOL)
            print('Zapisano przetransformowane dane do pliku.')
        except IOError as e:
            print('Błąd: problem podczas zapisywania do pliku:\n\t', e)
            log_and_quit()

        try:
            os.remove(EXTRACTED_DATA_FILENAME)
        except OSError:
            pass

    except FileNotFoundError:
        print('Błąd: należy uprzednio wykonać ekstrakcję danych!')
        log_and_quit()


def insert_one_record_into_db(record, timestamp):
    try:
        connection = sqlite3.connect(DATABASE_FILENAME)
        if connection is not None:
            with connection:
                cursor = connection.cursor()

                query = ' SELECT EXISTS(SELECT 1 FROM listings WHERE auction_id=' + record.auction_id + ')'
                cursor.execute(query)
                response = cursor.fetchall()
                if response[0][0] == 1:
                    return 0

                query = ''' INSERT INTO listings(auction_id,brand,model,category,year,price,mileage,
                                engine,fuel,location,dealer,origin_country,power,transmission,type,
                                doors,seats,color,first_registered,first_owner,no_accidents,state,
                                abs,electric_windscreens,power_steering,cruise_control,
                                gps,electric_side_mirrors,alarm,immobiliser,driver_airbag,aux,bluetooth,radio,timestamp)
                            VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) '''

                values_list = ['NULL' if record.items()[item] is None else record.items()[item] for item in list(record.items())]
                values_list.append(timestamp)
                values = tuple(values_list)
                cursor.execute(query, values)

                return 1
        else:
            print('Błąd: nie można uzyskać połączenia z bazą!')
            log_and_quit()
    except Error as e:
        print('Błąd w dostępie do bazy danych:\n\t', e)
        log_and_quit()


def load():
    print('Ładowanie wyników do bazy')
    timestamp = str(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S'))
    try:
        with open(TRANSFORMED_DATA_FILENAME, 'rb') as handle:
            records = pickle.load(handle)
            if len(records) == 0:
                print('Błąd: nie załadowano żadnych danych do pliku po procesie ekstrakcji.')
                log_and_quit()

        try:
            result = [insert_one_record_into_db(record, timestamp) for record in records]
            if sum(result) == len(result):
                print('Załadowano do bazy wszystkie', len(result), 'wyników.')
            elif sum(result) == 0:
                print('Nie załadowano do bazy żadnych rekordów, jako że wszystkie duplikowały już istniejące')
            else:
                print('Spośród', len(result), 'rekordów', sum(result), 'zostało załadowanych do bazy, '
                                                                       'resztę pominięto jako duplikaty')

        except AttributeError as e:
            print('Błąd: nieprawidłowy plik wynikowy z transformacji danych\n\t', e)
            log_and_quit()

        try:
            os.remove(TRANSFORMED_DATA_FILENAME)
        except OSError:
            pass

    except FileNotFoundError:
        print('Błąd: należy uprzednio wykonać transformację danych!')
        log_and_quit()


def etl(n_records, brand, model):
    extract(n_records, brand, model)
    transform()
    load()


def is_string_in_dict_values(string, dictionary):
    return string.lower() in [d.lower() for d in list(dictionary.values())]


def get_n_records_brand_model():
    user_input = input('Ile ofert ma zostać pobranych? ')
    try:
        n_records = int(user_input)

        user_input = input('Wybrana marka (numer lub nazwa marki):\n\t'
                           + '\n\t'.join([str(k) + ': ' + BRANDS[str(k)] for k in sorted(list(map(int, list(BRANDS.keys()))))]) + '\n')

        if user_input in BRANDS.keys():
            brand = BRANDS[user_input]
        elif is_string_in_dict_values(user_input, BRANDS):
            brand = user_input
        else:
            print('Błąd: nie została wybrana poprawna opcja!')
            log_and_quit()

        models = eval('MODELS_' + brand.upper())
        user_input = input('Wybrany model:\n\t'
                           + '\n\t'.join([str(k) + ': ' + models[str(k)] for k in sorted(list(map(int, list((models.keys())))))]) + '\n')

        if user_input in models.keys():
            model = models[user_input]
        elif is_string_in_dict_values(user_input, models):
            model = user_input
        else:
            print('Błąd: nie został wybrany poprawny model')
            log_and_quit()

        return [n_records, brand, model]

    except ValueError:
        print('Błąd: nie została wprowadzona poprawna liczba!')
        log_and_quit()


def configure_etl():
    try:
        etl(*get_n_records_brand_model())
    except TypeError as e:
        print('Błąd podczas wybierania marki i modelu\n\t', e)


def configure_extraction():
    try:
        extract(*get_n_records_brand_model())
    except TypeError as e:
        print('Błąd podczas wybierania marki i modelu\n\t', e)
        log_and_quit()


def display_db(n_records, brand, model):
    try:
        connection = sqlite3.connect(DATABASE_FILENAME)
        if connection is not None:
            with connection:
                cursor = connection.cursor()

                query = 'SELECT * FROM listings WHERE UPPER(brand)=\'' + brand.upper() + '\' AND UPPER(model)=\'' + model.upper() + '\' LIMIT ' + str(n_records)
                cursor.execute(query)
                response = cursor.fetchall()
                if len(response) == 0:
                    print('Brak rekordów w bazie danych.')
                else:
                    print(get_column_names(cursor))
                    for record in response:
                        print(record)
        else:
            print('Błąd: nie można uzyskać połączenia z bazą!')
            log_and_quit()
    except Error as e:
        print('Błąd w dostępie do bazy danych:\n\t', e)
        log_and_quit()


def configure_db_display():
    try:
        display_db(*get_n_records_brand_model())
    except TypeError as e:
        print('Błąd podczas wybierania marki i modelu\n\t', e)
        log_and_quit()


def save_db_to_csv(n_records, brand, model):
    try:
        if not os.path.exists(OUTPUT_DIRECTORY):
            os.makedirs(OUTPUT_DIRECTORY)
    except:
        print('Błąd podczas tworzenia katalogu.')
        log_and_quit()
    try:
        connection = sqlite3.connect(DATABASE_FILENAME)
        if connection is not None:
            with connection:
                cursor = connection.cursor()
                query = 'SELECT * FROM listings WHERE UPPER(brand)=\'' + brand.upper() + '\' AND UPPER(model)=\'' + model.upper() + '\' LIMIT ' + str(n_records)
                cursor.execute(query)
                response = cursor.fetchall()
                columns = get_column_names(cursor)
                try:
                    with open(OUTPUT_DIRECTORY + 'database.csv', 'w') as handle:
                        csv_out = csv.writer(handle)
                        csv_out.writerow(columns)
                        for record in response:
                            csv_out.writerow(record)
                except IOError as e:
                    print('Błąd: problem podczas zapisywania do pliku:\n\t', e)
                    log_and_quit()
        else:
            print('Błąd: nie można uzyskać połączenia z bazą!')
            log_and_quit()
    except Error as e:
        print('Błąd w dostępie do bazy danych:\n\t', e)
        log_and_quit()


def configure_save_db_to_csv():
    try:
        save_db_to_csv(*get_n_records_brand_model())
    except TypeError as e:
        print('Błąd podczas wybierania marki i modelu\n\t', e)
        log_and_quit()


def replace_null(pairs):
    return [[pair[0], 'brak danych' if pair[1] == 'NULL' else pair[1]]for pair in pairs]


def save_db_records_to_files(n_records, brand, model):
    try:
        if not os.path.exists(OUTPUT_DIRECTORY):
            os.makedirs(OUTPUT_DIRECTORY)
    except:
        print('Błąd podczas tworzenia katalogu.')
        log_and_quit()

    try:
        connection = sqlite3.connect(DATABASE_FILENAME)
        if connection is not None:
            with connection:
                cursor = connection.cursor()
                query = 'SELECT * FROM listings WHERE UPPER(brand)=\'' + brand.upper() + '\' AND UPPER(model)=\'' + model.upper() + '\' LIMIT ' + str(n_records)
                cursor.execute(query)
                response = cursor.fetchall()
                columns = get_column_names(cursor)
                if len(response) > 0:
                    for record in response:
                        output = replace_null(list(zip(columns, record)))
                        try:
                            with open(OUTPUT_DIRECTORY + record[0] + '.out', 'w') as handle:
                                for item in output:
                                    handle.write(':\t\t'.join(item) + '\n')
                        except IOError as e:
                            print('Błąd podczas zapisywania do pliku!\n\t', e)
                print('Plików zapisanych:', len(response))
        else:
            print('Błąd: nie można uzyskać połączenia z bazą!')
            log_and_quit()
    except Error as e:
        print('Błąd w dostępie do bazy danych:\n\t', e)
        log_and_quit()


def configure_save_db_records_to_files():
    try:
        save_db_records_to_files(*get_n_records_brand_model())
    except TypeError as e:
        print('Błąd podczas wybierania marki i modelu\n\t', e)
        log_and_quit()


def clear_db():
    try:
        connection = sqlite3.connect(DATABASE_FILENAME)
        if connection is not None:
            with connection:
                cursor = connection.cursor()
                cursor.execute('DELETE FROM listings')
        else:
            print('Błąd: nie można uzyskać połączenia z bazą!')
            log_and_quit()
    except Error as e:
        print('Błąd w dostępie do bazy danych:\n\t', e)
        log_and_quit()


def create_table(connection, create_table_query):
    try:
        c = connection.cursor()
        c.execute(create_table_query)
    except Error:
        print('Błąd podczas tworzenia tabeli')
        log_and_quit()


def connect_to_db():
    try:
        connection = sqlite3.connect(DATABASE_FILENAME)
        if connection is not None:
            with connection:
                create_table(connection, CREATE_TABLE)
        else:
            print('Błąd: nie można uzyskać połączenia z bazą!')
            log_and_quit()
    except Error as e:
        print('Błąd w dostępie do bazy danych:\n\t', e)
        log_and_quit()


def get_column_names(cursor):
    cursor.execute("PRAGMA table_info(listings)")
    all_records = cursor.fetchall()
    column_names = []
    for record in all_records:
        column_names.append(record[1])
    return tuple(column_names)


def select_all_from_db():
    try:
        connection = sqlite3.connect(DATABASE_FILENAME)
        if connection is not None:
            with connection:
                cursor = connection.cursor()
                cursor.execute("SELECT * FROM listings")
                all_records = cursor.fetchall()
                if len(all_records) == 0:
                    print('Brak rekordów w bazie!')
                else:
                    print(get_column_names(cursor))
                    for record in all_records:
                        print(record)
        else:
            print('Błąd: nie można uzyskać połączenia z bazą!')
            log_and_quit()
    except Error as e:
        print('Błąd w dostępie do bazy danych:\n\t', e)
        log_and_quit()


def main():
    main_menu = \
        {
            '0': configure_etl,
            '1': configure_extraction,
            '2': transform,
            '3': load,
            '4': configure_db_display,
            '5': configure_save_db_to_csv,
            '6': configure_save_db_records_to_files,
            '7': clear_db,
            '8': select_all_from_db
        }

    connect_to_db()

    user_choice = input('Jaka operacja ma zostać przeprowadzona?\n'
                        '\t0 - Cały proces ETL\n'
                        '\t1 - Ekstrakcja danych\n'
                        '\t2 - Transformacja danych (wcześniejsza ekstrakcja wymagana)\n'
                        '\t3 - Ładowanie danych (wcześniejsza transformacja wymagana)\n'
                        '\t4 - Wyświetlanie rekordów z bazy danych\n'
                        '\t5 - Zapis bazy do pliku CSV\n'
                        '\t6 - Zapis rekordów do osobnych plików\n'
                        '\t7 - Wyczyszczenie bazy danych\n'
                        '\t8 - Wyświetlanie całej tabeli\n')

    try:
        main_menu[user_choice]()
    except KeyError:
        print('Błąd: nie została wybrana poprawna opcja!')
        log_and_quit()


if __name__ == '__main__':
    main()
